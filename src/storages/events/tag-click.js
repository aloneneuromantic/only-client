const Leaf = require('../../tree/leaf.js');
// class for events
class TagClick extends Leaf {
    constructor(trunk, sprout, rame, elm) {
        super(trunk, sprout, rame);
        this.date = new Date();
        this.tag = elm.target.localName;
        if (this.tag === 'a') {
            this.innerText = elm.target.innerText;
            this.href = elm.target.href;
        }
        this.id = elm.target.id;
        this.className = elm.target.className;
    }
}

module.exports = TagClick;