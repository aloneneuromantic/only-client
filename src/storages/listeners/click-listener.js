const Rame = require('../../tree/rame.js'),
    TagClick = require('../events/tag-click.js');
// Click listener object
class ClickListener extends Rame {
    constructor(trunk, sprout, obj) {
        super(trunk, sprout);
        if (obj.type === 'click') {
            if (obj.state === 'id' && obj.value === null) {
                document.onclick = (e) => {
                    if (e.target.id && obj.value === null) {
                        this.pushLeaf(new TagClick(trunk, sprout, this._id, e));
                    }
                };
            } else if (state === 'id' && value !== null) {
                document.onclick = (e) => {
                    if (e.target.id === value) {
                        this.pushLeaf(new TagClick(trunk, sprout, this._id, e));
                    }
                };
            } else if (state === 'class' && value === null) {
                document.onclick = (e) => {
                    if (e.target.className) {
                        this.pushLeaf(new TagClick(trunk, sprout, this._id, e));
                    }
                };
            } else if (state === 'class' && value !== null) {
                document.onclick = (e) => {
                    if (e.target.className === value) {
                        this.pushLeaf(new TagClick(trunk, sprout, this._id, e));
                    }
                };
            } else {
                document.onclick = (e) => {
                    this.pushLeaf(new TagClick(trunk, sprout, this._id, e));
                };
            }
        } else if (obj.type === 'inherit-click') {
            document.onclick = (e) => {
                if (e.target.parentElement.id) {
                    if (obj.state === 'id') {
                        if (obj.value === e.target.parentElement.id) {
                            this.pushLeaf(new TagClick(trunk, sprout, this._id, e));
                        }
                    } else if (obj.state === 'class') {
                        e.target.parentElement.className.split(' ').forEach((c) => {
                            if (c === obj.value) {
                                this.pushLeaf(new TagClick(trunk, sprout, this._id, e));
                            }
                        });
                    }
                }
            };
        }
    }
}

module.exports = ClickListener;