const Sprout = require('../tree/sprout.js'),
    ClickListener = require('./listeners/click-listener.js');
// Class for event storage
class EventStorage extends Sprout {
    constructor(trunk, obj) {
            super(trunk);
            this.date = new Date();
            this.url = location.hostname + location.pathname + location.search;
            if (obj.type === 'click' || obj.type === 'inherit-click') {
                this.pushRame(new ClickListener(trunk, this._id, obj));
            }
            this.name = "EventStorage";
        }
        // Main API
    getListenerList() {
        return this.rames;
    }
    getEventList(index) {
        return this.rames[index].leafs;
    }
}

module.exports = EventStorage;