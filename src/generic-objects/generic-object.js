// GENERIC OBJECT
class GenericObject {
    constructor() {
        this._id = `obj-${Math.floor(Math.random() * (9999 - 10 + 1)) + 10}`;
        this.date = new Date();
    }
    get(value) {
        return this[value];
    }
    set(wtf, value) {
        this[wtf] = value;
    }
}

module.exports = GenericObject;