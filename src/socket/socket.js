const Sprout = require('../tree/sprout.js'),
    SocketEvent = require('./events/socket-event.js');
// Class for sockets
class Socket extends Sprout {
    constructor(trunk, obj) {
            super(trunk._id);
            this.name = "Socket";
            this.time = new Date();
            this.connection = obj.lib(obj.url);
            this.onList = [];
            this.emitList = [];
            this.unknownEvens = [];
            if (obj.events.length !== 0 && obj.events !== undefined) {
                obj.events.forEach((socketEvent) => {
                    this.pushRame(new SocketEvent(trunk, this._id, socketEvent, this.connection));
                });
            }
            this.wateringSprout();
        }
        // Main API
    wateringSprout() {
        this.rames.forEach((rame) => {
            if (rame.socketEvent === 'on') {
                this.onList.push(rame);
            } else if (rame.socketEvent === 'emit') {
                this.emitList.push(rame);
            }
        });
    }
    getEventList() {
        return this.rames;
    }
}

module.exports = Socket;