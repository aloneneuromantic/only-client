const Rame = require('../../tree/rame.js');
// Class for socket event listener
class SocketEvent extends Rame {
    constructor(trunk, sprout, obj, connect) {
            super(trunk._id, sprout);
            this.date = new Date();
            this.connect = connect;

            let counter = 0;


            ['message',
                'socketEvent',
                'callback'
            ].forEach((item) => {
                this[item] = obj[item];
                counter++;
                if (counter === 3) {
                    if (obj.socketEvent === 'on') {
                        if (this.callback !== null) {
                            connect.on(this.message, (msg) => {
                                this.callback(msg);
                            });
                        } else {
                            connect.on('snd', (msg) => {
                                trunk.getFormsStorage().updateForms(msg);
                            });
                        }
                    } else {
                        console.log('emit');
                    }
                }
            });
        }
        // Main API
    emit(wtf) {
        this.connect.emit(this.message, this.connect.id, wtf);
    }
}

module.exports = SocketEvent;