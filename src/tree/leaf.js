const GenericObject = require('../generic-objects/generic-objects.js');
// Leaf for application
class Leaf extends GenericObject {
    constructor(trunk, sprout, rame) {
        super();
        this._id = `leaf-${Math.floor(Math.random() * (9999 - 10 + 1)) + 10}`;
        this._trunk = trunk;
        this._sprout = sprout;
        this._rame = rame;
    }
}

module.exports = Leaf;