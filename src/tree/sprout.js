const GenericObject = require('../generic-objects/generic-object.js');
// Sprout for application
class Sprout extends GenericObject {
    constructor(trunk) {
        super();
        this._id = this._id = `sprout-${Math.floor(Math.random() * (999999 - 10 + 1))+10}`;
        this._trunk = trunk;
        this.rames = [];
    }
    pushRame(rame) {
        this.rames.push(rame);
    }
}

module.exports = Sprout;