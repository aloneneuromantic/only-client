const Sprout = require('./sprout.js');
// Rame for application
class Rame extends Sprout {
    constructor(trunk, sprout) {
        super(trunk);
        this._id = `rame-${Math.floor(Math.random() * (9999 - 10 + 1)) + 10}`;
        this._sprout = sprout;
        this.leafs = [];
    }
    pushLeaf(leaf) {
        this.leafs.push(leaf);
    }
}

module.exports = Rame;