const GenericObject = require('../generic-objects/generic-object.js');
// Main application OBJECT
class Trunk extends GenericObject {
    constructor(obj) {
        super();

        this._id = `trunk-${Math.floor(Math.random() * (9999 - 10 + 1)) + 10}`;
        this.sprouts = [];

        if (obj.addVisitor === true) {
            this.pushSprout(new Visitor(this._id));
        }

        if (obj.addEventStorage.length !== 0 && obj.addEventStorage.length !== undefined) {
            obj.addEventStorage.forEach((listener) => {
                this.pushSprout(new EventStorage(this._id, listener));
            });
        }

        if (obj.addSocket.state === true) {
            this.pushSprout(new Socket(this, obj.addSocket));
        }

        if (obj.getForms.state === true) {
            this.pushSprout(new FormStorage(this._id, obj.getForms));
        }

        this.wateringTrunk();
    }
    pushSprout(sprout) {
            this.sprouts.push(sprout);
        }
        // Main API
    wateringTrunk() {
        this.sprouts.forEach((e) => {
            [Visitor,
                Socket,
                EventStorage,
                FormStorage
            ].forEach((c) => {
                if (e instanceof c) {
                    this[`${c.name}`] = e;
                }
            });
        });
    }
    addFormStorage(obj) {
        return new Promise((resolve) => {
            this.pushSprout(new FormStorage(this._id, obj));
            resolve(true);
        });
    }
    getVisitor() {
        return this.Visitor;
    }
    getSocket() {
        return this.Socket;
    }
    getEventStorage() {
        return this.EventStorage;
    }
    getFormsStorage() {
        if (this.FormStorage) {
            return this.FormStorage;
        } else {
            return this.sprouts[1];
        }
    }
    regetFormValues(form) {
        return new Promise((resolve) => {
            let count = 0,
                max = form.leafs.length - 1;
            form.leafs.forEach((input) => {
                input.getValues();
                if (count === max) {
                    resolve(true);
                } else {
                    count = count + 1;
                }
            });
        });
    }
    regetFormsValues() {
        return new Promise((resolve) => {
            let count = 0,
                max = this.getFormsStorage().getFormsList().length - 1;
            this.getFormsStorage().getFormsList().forEach((form) => {
                this.regetFormValues(form)
                    .then(() => {
                        if (count === max) {
                            resolve(true);
                        } else {
                            count = count + 1;
                        }
                    });
            });
        });
    }
    emit(obj) {
        console.log('EMIT');
        if (obj.reget === false) {
            this.Socket.emitList[obj.index].emit(JSON.stringify(this.getFormsStorage().getFormsList()));
        } else {
            this.regetFormsValues().then(() => {
                this.Socket.emitList[obj.index].emit(JSON.stringify(this.getFormsStorage().getFormsList()));
            });
        }
    }
}