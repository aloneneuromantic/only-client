const Rame = require('../tree/rame.js'),
    BrowserInfo = require('./device/browser-info.js'),
    ScreenInfo = require('./device/screen-info.js');

class DeviceInfo extends Rame {
    constructor(trunk, sprout) {
        super(trunk, sprout);
        let userAgent = navigator.userAgent;
        this.type = this.getDeviceType(userAgent);
        this.os = this.getDeviceOs(userAgent);
        [
            new BrowserInfo(trunk, sprout, this._id),
            new ScreenInfo(trunk, sprout, this._id)
        ].forEach((item) => {
            this.pushLeaf(item);
        });
    }
    getDeviceType(ua) {
        if (ua.match(/iPhone/) ||
            ua.match(/BlackBerry/) ||
            ua.match(/(Windows Phone OS|Windows CE|Windows Mobile)/) ||
            ua.match(/Mobile/) ||
            ua.match(/(Opera Mini|IEMobile|SonyEricsson|smartphone)/)) {
            return 'mobile';
        } else if (ua.match(/iPod/) ||
            ua.match(/iPad/) ||
            ua.match(/PlayBook/) ||
            ua.match(/(GT-P1000|SGH-T849|SHW-M180S)/) ||
            ua.match(/Tablet PC/) ||
            ua.match(/(PalmOS|PalmSource| Pre\/)/) ||
            ua.match(/(Kindle)/)) {
            return 'tablet';
        } else {
            return 'desktop';
        }
    }
    getDeviceOs(ua) {
        if (this.type === 'desktop') {
            if (ua.search(/Windows/) > -1) {
                let tmp = ua.toLowerCase();
                //
                if (tmp.indexOf('windows nt 5.0') > 0) return 'Microsoft Windows 2000';
                if (tmp.indexOf('windows nt 5.1') > 0) return 'Microsoft Windows XP';
                if (tmp.indexOf('windows nt 5.2') > 0) return 'Microsoft Windows Server 2003 or Server 2003 R2';
                if (tmp.indexOf('windows nt 6.0') > 0) return 'Microsoft Windows Vista or Server 2008';
                if (tmp.indexOf('windows nt 6.1') > 0) return 'Microsoft Windows 7 or Server 2008';
                if (tmp.indexOf('windows nt 6.2') > 0) return 'Microsoft Windows 8 or Server 2012';
                if (tmp.indexOf('windows nt 6.3') > 0) return 'Microsoft Windows 8.1 or Server 2012 R2';
                if (tmp.indexOf('windows nt 10') > 0) return 'Microsoft Windows 10 or Server 2016';

                return 'UnknownWindowsVersion';
            } else {
                if (ua.search('Linux') > -1) return 'Linux';
                if (ua.search('Macintosh') > -1) return 'Macintosh';
                if (ua.search('Mac OS X') > -1) return 'Mac OS X';

                return 'UnknownDesktopeOperationSystem';
            }
        } else {
            if (ua.match(/iPhone/) || ua.match(/iPod/) || ua.match(/iPhone/) && !window.MSStream) return 'iOS';
            if (ua.match(/BlackBerry/)) return 'BlackBerry OS';
            if (ua.match(/(Windows Phone OS|Windows CE|Windows Mobile)/)) return 'Windows Phone';
            if (ua.match(/Android/)) return 'Android';

            return 'UnknownMobileOperationgSystem';
        }
    }
}

module.exports = DeviceInfo;