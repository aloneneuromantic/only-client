const Rame = require('../tree/rame.js');
// // Class for dateInfo
class DateInfo extends Rame {
    constructor(trunk, sprout) {
        super(trunk, sprout);
        let date = new Date();
        this.openDate = date;
        this.timezone = date.getTimezoneOffset() / 60;
    }
}

module.exports = DateInfo;