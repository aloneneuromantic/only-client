const Rame = require('../tree/rame.js');
// Class for location
class LocationInfo extends Rame {
    constructor(trunk, sprout) {
        super(trunk, sprout);
        ['hash',
            'hostname',
            'href',
            'origin',
            'pathname',
            'port',
            'protocol',
            'search'
        ].forEach((item) => {
            this[item] = location[item];
        });
        this.fullUrl = location.hostname + location.pathname + location.search;
        this.pageCounter = history.length;
        this.refferer = document.refferer;
    }
}

module.exports = LocationInfo;