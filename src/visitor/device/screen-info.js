const Leaf = require('../tree/leaf.js');
// Class of screen
class ScreenInfo extends Leaf {
    constructor(trunk, sprout, rame) {
        super(trunk, sprout, rame);
        ['width',
            'height',
            'availWidth',
            'availHeight',
            'colorDepth',
            'pixelDepth'
        ].forEach((item) => {
            this[item] = screen[item];
        });
        ['width', 'height'].forEach((item) => {
            this[`${item}Document`] = document[item];
        });
        this.innerWidth = innerWidth;
        this.innerHeight = innerHeight;
    }
}

module.exports = ScreenInfo;