const Leaf = require('../tree/leaf.js');
// Browser information
class BrowserInfo extends Leaf {
    constructor(trunk, sprout, rame) {
        super(trunk, sprout, rame);
        let nav = navigator;
        this.detectedName = this.detectBrowserName(nav.userAgent);
        ['appName',
            'appVersion',
            'userAgent',
            'product',
            'language',
            'languages',
            'onLine',
            'platform',
            'cookieEnabled'
        ].forEach((item) => {
            this[item] = nav[item];
        });
        this.javaEnabled = nav.javaEnabled();
        this.cookieFirst = document.cookie;
        this.cookiesSecond = decodeURIComponent(document.cookie.split(";"));
        this.localStorage = localStorage;
    }
    detectBrowserName(ua) {
        if (ua.search(/MSIE/) > -1) return 'InternetExplorer';
        if (ua.search(/Trident/) > -1) return 'InternetExplorer(Trident)';
        if (ua.search(/OPR/) > -1) return 'NewOpera';
        if (ua.search(/Yowser/) > -1) return 'YandexBrowser';
        if (ua.search(/UBrowser/) > -1) return 'UCBrowser';
        if (ua.search(/SeaMonkey/) > -1) return 'SeaMonkey';
        if (ua.search(/Iceweasel/) > -1) return 'IceWeasel';
        if (ua.search(/Opera/) > -1) return 'OldOpera';
        if (ua.search(/Firefox/) > -1) return 'Firefox';
        if (ua.search(/Vivaldi/) > -1) return 'Vivaldi';
        if (ua.search(/Edge/) > -1) return 'Edge';
        if (ua.search(/Safari/) > -1 && navigator.vendor.indexOf('Apple') > -1 && ua && ua.match('CriOS')) return 'Safari';
        if (ua.search(/Konqueror/) > -1) return 'Konqueror';
        if (ua.search(/Chrome/) > -1) return 'GoogleChrome';

        return 'UnknownBrowser';
    }
}

module.exports = BrowserInfo;