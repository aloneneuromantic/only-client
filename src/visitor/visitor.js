const Sprout = require('../tree/sprout.js'),
    DateInfo = require('./date-info.js'),
    DeviceInfo = require('./device-info.js'),
    LocationInfo = require('./location-info.js');
// class for client information
class Visitor extends Sprout {
    constructor(trunk) {
            super(trunk);
            [
                new DateInfo(trunk, this._id),
                new LocationInfo(trunk, this._id),
                new DeviceInfo(trunk, this._id)
            ].forEach((rame) => {
                this.pushRame(rame);
            });
            this.name = "Visitor";
        }
        // Main API
    getDateInfo() {
        return this.rames[0];
    }
    getLocationInfo() {
        return this.rames[1];
    }
    getDeviceInfo() {
        return this.rames[2];
    }
}

module.exports = Visitor;